package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String ordenar){
        System.out.println("findAll UserService");

        List<UserModel> result;

        if(ordenar.equalsIgnoreCase("si")){
            result = this.userRepository.findAll(Sort.by(Sort.Direction.ASC,"age"));
        }
        else{
            result = this.userRepository.findAll();
        }
        return result;
    }

    public Optional<UserModel> findById(String id){
        System.out.println("findById UserService");

        return this.userRepository.findById(id);
    }

    public UserModel add(UserModel user){
        System.out.println("add UserService");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user){
        System.out.println("update UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("delete UserService");
        boolean result = false;

        if(this.findById(id).isPresent()){
            System.out.println("Usuario encontrado, borrando");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}
