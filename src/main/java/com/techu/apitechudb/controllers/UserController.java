package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apiusers/v1")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "ordenar", defaultValue = "no", required = false) String ordenar){
        System.out.println("getUsers");
        System.out.println("Hay peticion de ordenacion?" + ordenar);

        return new ResponseEntity<>(
                this.userService.findAll(ordenar)
                , HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encotrado"
                , result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");

        return new ResponseEntity<>(
                this.userService.add(user)
                , HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@PathVariable String id, @RequestBody UserModel user){
        System.out.println("updateUser");

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if(userToUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado, actualizando");
            if(id.equalsIgnoreCase(user.getId())) {
                this.userService.update(user);
            }
        }

        return new ResponseEntity<>(
                 user
                , userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no borrado"
                , deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}

